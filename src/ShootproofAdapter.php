<?php

namespace Albumparrot\Flysystem;

use League\Flysystem\Adapter\AbstractAdapter;
use League\Flysystem\Adapter\Polyfill\NotSupportingVisibilityTrait;
use League\Flysystem\Config;
use League\Flysystem\Util;
use Sp_Api as Client;

class ShootproofAdapter extends AbstractAdapter
{
    use NotSupportingVisibilityTrait;

    /**
     * @var array
     */
    protected static $resultMap = [
        'size' => 'size',
        'mime_type' => 'mime_type',
        'width' => 'width',
        'height' => 'height',
        'name' => 'filename',
        'url' => 'url', 
        'cover_photo' => 'cover_photo'
    ];

    /**
     * @var Client
     */
    protected $client;

    /**
     * Constructor.
     *
     * @param Client $client
     * @param string $prefix
     */
    public function __construct(Client $client, $prefix = '/')
    {
        $this->client = $client;
        $this->setPathPrefix($prefix);
    }

    /**
     * {@inheritdoc}
     */
    public function has($path)
    {
        return $this->getMetadata($path);
    }

    /**
     * {@inheritdoc}
     */
    public function write($path, $contents, Config $config)
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function writeStream($path, $resource, Config $config)
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function update($path, $contents, Config $config)
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function updateStream($path, $resource, Config $config)
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function read($path)
    {
        if ( ! $object = $this->readStream($path)) {
            return false;
        }

        $object['contents'] = stream_get_contents($object['stream']);
        fclose($object['stream']);
        unset($object['stream']);

        return $object;
    }

    /**
     * {@inheritdoc}
     */
    public function readStream($path)
    {
        $stream = fopen('php://temp', 'w+');
        $location = $this->applyPathPrefix($path);

        if ( ! $this->client->getFile($location, $stream)) {
            fclose($stream);

            return false;
        }

        rewind($stream);

        return compact('stream');
    }

    /**
     * {@inheritdoc}
     */
    public function rename($path, $newpath)
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function copy($path, $newpath)
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function delete($path)
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function deleteDir($path)
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function createDir($path, Config $config)
    {
        throw new BadMethodCallException('Not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function getMetadata($path)
    {
        $directory = $this->listContents(Util::dirname($path), false);
        $file = [];
        $key = array_search($path, array_column($directory, 'path'));
            
        if ($key !== false) {
            $file = $directory[$key];
        }

        return $file;
    }

    /**
     * {@inheritdoc}
     */
    public function getMimetype($path)
    {
        return $this->getMetadata($path);
    }

    /**
     * {@inheritdoc}
     */
    public function getSize($path)
    {
        return $this->getMetadata($path);
    }

    /**
     * {@inheritdoc}
     */
    public function getTimestamp($path)
    {
        return $this->getMetadata($path);
    }

    /**
     * {@inheritdoc}
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * {@inheritdoc}
     * event/album/photo
     */
    public function listContents($directory = '', $recursive = false)
    {
        $prefix = $this->applyPathPrefix(rtrim($directory, '/') . '/');
        if (strrpos($directory, '/') !== false) {
            $location = substr($directory, strrpos($directory, '/') + 1);
        } else {
            $location = $directory;
        }
        
        $listing = [];
        $type = 'root';
        $identifier = '';
        if (strpos($directory, ':') !== false) {
            list($type, $identifier) = explode(':', $location);
        }
        $result = false;
        
        switch ($type) {
            case 'event':
                $result = $this->getEventAlbums($identifier, $prefix);
                break;
            case 'album':
                $result = $this->getAlbumPhotos($identifier, $prefix);
                break;
            default:
                $result = $this->getEvents($prefix);
                break;
        }
        
        foreach ($result as $object) {
            $path = $object['path'];
            
            $listing[] = $this->normalizeResponse($object, $path);

            if ($recursive && $object['type'] == 'dir') {
                $listing = array_merge($listing, $this->listContents($path, true));
            }
        }

        return $listing;
    }
    
    protected function getEvents($prefix)
    {
        if (! $result = $this->client->getEvents() ) {
            return [];
        }

        if ($result['stat'] == 'ok') {
            foreach ($result['events'] as &$event) {
                $event['path'] = $prefix.'/event:'.$event['id'];
                $event['type'] = 'dir';
                $event['is_dir'] = true;
            }

            return $result['events'];
        }
    }
    
    protected function getEventAlbums($identifier, $prefix)
    {
        $photos = [];

        $result = $this->client->getEventAlbums($identifier);
        if ($result && $result['stat'] == 'ok') {
            foreach ($result['albums'] as $album) {
                $album['path'] = $prefix.'/album:'.$album['id'];
                $album['type'] = 'dir';
                $album['is_dir'] = true;

                $photos[] = $album;
            }
        }

        $result = $this->client->getEventPhotos($identifier);
        if ($result && $result['stat'] == 'ok') {
            foreach ($result['photos'] as $photo) {
                $photo['path'] = $prefix.'/photo:'.$photo['id'];
                $photo['type'] = 'file';
                $photo['is_dir'] = false;

                $photos[] = $photo;
            }
        }

        return $photos;
    }
    
    protected function getAlbumPhotos($identifier, $prefix)
    {
        if (! $result = $this->client->getAlbumPhotos($identifier) ) {
            return [];
        }

        if ($result['stat'] == 'ok') {
            foreach ($result['photos'] as &$photo) {
                $photo['path'] = $prefix.'/photo:'.$photo['id'];
                $photo['type'] = 'file';
                $photo['is_dir'] = false;
            }

            return $result['photos'];
        }
    }

    /**
     * Apply the path prefix.
     *
     * @param string $path
     *
     * @return string prefixed path
     */
    public function applyPathPrefix($path)
    {

        $path = parent::applyPathPrefix($path);

        return '/' . ltrim(rtrim($path, '/'), '/');
    }

    /**
     * Normalize a Dropbox response.
     *
     * @param array $response
     *
     * @return array
     */
    protected function normalizeResponse(array $response)
    {
        $result = ['path' => ltrim($this->removePathPrefix($response['path']), '/')];

        if (isset($response['modified'])) {
            $result['timestamp'] = strtotime($response['modified']);
        }
        
        $result['dirname'] = Util::dirname($result['path']);
        
        $result['type'] = $response['type'];

        $result = array_merge($result, Util::map($response, static::$resultMap));
        
        return $result;
    }
}
